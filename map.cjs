function map(elements, cb) {
    if (Array.isArray(elements)) {
        const result = [];
        for (let index = 0; index < elements.length; index++) {
            const mappedValue = cb(elements[index], index, elements);
            result.push(mappedValue);
        }
        return result;
    }
    return []; // Return an empty array for non-array inputs
}

module.exports = map;
