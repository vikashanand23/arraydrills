function flatten(elements, depth = 1) {
  if (depth === 0) {
    return elements.slice();
  }

  let result = [];

  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      result = result.concat(flatten(elements[index], depth - 1));
    } else if (elements[index] !== undefined) {
      result.push(elements[index]);
    }
  }

  return result;
}

module.exports = flatten;
