function filter(elements, cb){
    let result = [];
    if (Array.isArray(elements)){
        for( let index = 0; index < elements.length; index++){
            if ( cb(elements[index], index, elements) === true ) {
                result.push(elements[index]);
            }
        } 
        return result;
    }
    return [];
    
}

module.exports = filter;
