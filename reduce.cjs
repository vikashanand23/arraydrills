function reduce(elements, cb, startingValue) {
    if (elements.length === 0) {
        throw new TypeError("Empty array");
    }

    let sum = startingValue !== undefined ? startingValue : elements[0];

    for (let index = startingValue !== undefined ? 0 : 1; index < elements.length; index++) {
        sum = cb(sum, elements[index], index, elements);
    }

    return sum;
}

module.exports = reduce;
