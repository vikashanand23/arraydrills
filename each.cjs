function each(elements, cb){   // function named each to iterate throuch each element
    if (Array.isArray(elements)){
        for ( let index = 0; index < elements.length; index++){
            cb(elements[index])   //callback function to call the child function on invoking parent function
        }
    }
}

module.exports = each;
