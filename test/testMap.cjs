const map = require('../map.cjs'); // Importing the function
const items = [1, 2, 3, 4, 5];      // Define an array

const result = map(items, (item, index) => { // Invoking the map function
    return item ** 2;
});

console.log(result); // Print the result
