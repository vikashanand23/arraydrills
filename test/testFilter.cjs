const filter = require('../filter.cjs'); //Importing function
const items = [1, 2, 3, 4, 5];  //define an array
const result = filter( items, function( item, index, items ){   // invoking parent function
    return item > 4;
})

console.log(result);  //printing result
